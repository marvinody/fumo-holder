# DIY Fumo Stand
Disclaimer: I never really 3d modeled before this but I wanted to try at making these in a simple way. 3D printing came to mind and I went with it.

[Imgur Album](https://imgur.com/a/JMvw5Ba) of it holding up a meido

## To make your own:
  * Download the two .obj files
  * Get them printed (I used 3D hubs and a printer near me)
  * Also get some pipecleaners
  * Both the head and leg piece have 2 holes each that a pipe cleaner will fit through, so clip a couple short enough and wrap them around
  
  * Sit your fumo on the leg plate and slide the head plate between the head and hair
  * Optionally use another pipecleaner to secure the legs if you can hide it
  